﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodWaves : MonoBehaviour
{
    public Food[] HealthyFood;
    public Food[] UnhealthyFood;
    public GameObject[] SpawnPoints;
    public GameObject FoodPrefab;

    //Max 6.5f and 2 by default
    private float speed = 2f;
    //By default 1 and min 0.1
    private float spawnFreq = 1f;
    private float spawnFreqTMP = 1f;
    private float spawnFreqDefaultValue = 1f;
    //Min value 10% to appear, max value 50%
    private float unhealthyFoodProb = 10f;
    private int lastSpawnPoint;


    public void Update()
    {
        if (spawnFreq >= spawnFreqDefaultValue)
        {
            //Spawn food
            var rndSpawn = GetRandomSpawnPoint();
            var rndHealthyFood = GetRandomHealthyFood();
            var foodType = GetRandomFoodType(rndHealthyFood);
            CreateFood(rndHealthyFood, foodType, rndSpawn);
            spawnFreq = 0;
            CheckSpawnValue();
        }
        else
        {
            spawnFreq += Time.deltaTime;
        }
    }

    /// <summary>
    /// If the saved value has changed the actual spawnFrew will be updated
    /// </summary>
    private void CheckSpawnValue()
    {
        if(spawnFreqDefaultValue != spawnFreqTMP)
        {
            var debug = spawnFreqDefaultValue;
            spawnFreqDefaultValue = spawnFreqTMP;
            Debug.Log("Freq pasa de " + debug + " a " + spawnFreqTMP);
        }
    }

    public void IncreaseUnhealthyFoodProb(float prob)
    {
        unhealthyFoodProb += prob;
        //Debug.Log("UnhealthyFood prob" + unhealthyFoodProb);
    }

    public void IncreaseFoodSpeed(float speedIncrement)
    {
        speed += speedIncrement;
        //Debug.Log("Actual velocity" + speed);
    }

    public void DecreaseSpawnTime(float num)
    {
        var miau = spawnFreqTMP * 10;
        var guau = num * 10;
        spawnFreqTMP = (miau - guau) / 10;
        //Debug.Log("Spawn time is: " + spawnFreqTMP);
    }

    public float GetUnhealthyProb()
    {
        return unhealthyFoodProb;
    }

    public float GetFoodSpeed()
    {
        return speed;
    }

    public float GetSpawnTime()
    {
        return spawnFreqTMP;
    }

    #region GetRandomValues
    public int GetRandomFoodType(bool healthyFood)
    {
        int returnValue = 0;
        if(healthyFood)
        {
            var arrayLength = HealthyFood.Length;
            returnValue = Random.Range(0, arrayLength);
        }
        else
        {
            var arrayLength = UnhealthyFood.Length;
            returnValue = Random.Range(0, arrayLength);
        }
        return returnValue;
    }

    public bool GetRandomHealthyFood()
    {
        var healthy = Random.Range(0,101);
        if(healthy <= unhealthyFoodProb)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int GetRandomSpawnPoint()
    {
        int rndSpawn = lastSpawnPoint;
        var numSpawn = SpawnPoints.Length;
        while (rndSpawn == lastSpawnPoint)
        {
            rndSpawn = Random.Range(0, numSpawn);
        }
        lastSpawnPoint = rndSpawn;
        return rndSpawn;
    }
    #endregion

    public void CreateFood(bool healthyFood, int foodType, int spawnPointIndex)
    {
        GameObject foodItem = Instantiate(FoodPrefab);
        foodItem.transform.position = SpawnPoints[spawnPointIndex].transform.position;
        if(healthyFood)
        {
            foodItem.GetComponent<SpriteRenderer>().sprite = HealthyFood[foodType].FoodImage;
        }
        else
        {
            foodItem.GetComponent<SpriteRenderer>().sprite = UnhealthyFood[foodType].FoodImage;
        }
        foodItem.GetComponent<FoodBehavior>().FoodSpeed = speed;
        foodItem.GetComponent<FoodBehavior>().HealthyFood = healthyFood;
    }
}
