﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PlayerMovement Player;
    public TextMeshProUGUI PointText;
    public FoodWaves FoodWavesController;
    //Values to increment o decrement 
    public float SpeedIncrement;
    //Default to 0.1
    public float SpawnFreqDecrement;
    public float UnhealthyFoodProb;

    private float points = 0;
    private float oldPoints = 0;

    private void Start()
    {
        Player.eatHealthy += AddPoints;
        Player.eatUnhealthy += AddPoop;
    }

    private void AddPoints()
    {
        //Default add 10 points
        points += 10;
        PointText.text = points.ToString();
        //TODO uncomment
        CheckActualScore();
    }

    private void AddPoop()
    {
        if(points < 0)
        {
            points = 0;
        }
        PointText.text = points.ToString();
        //test line
        //CheckActualScore();
    }

    private void CheckActualScore()
    {
        //Every 30 points the difficulty gets increased
        var pointsDiff = CheckLastScore(points);
        if(pointsDiff)
        {
            //Debug.Log(points + " es multiplo de 30");
            if (FoodWavesController.GetFoodSpeed() < 6.5f)
            {
                FoodWavesController.IncreaseFoodSpeed(SpeedIncrement);
            }
            var spawnTime = FoodWavesController.GetSpawnTime();
            //TODO try with 0.15 tested and it works properly
            if (FoodWavesController.GetSpawnTime() > 0.15f)
            {
                FoodWavesController.DecreaseSpawnTime(SpawnFreqDecrement);
            }
            if (FoodWavesController.GetUnhealthyProb() < 60f)
            {
                FoodWavesController.IncreaseUnhealthyFoodProb(UnhealthyFoodProb);
            }
        }
    }

    private bool CheckLastScore(float points)
    {
        var pointsDiff = points - oldPoints;
        if(pointsDiff >= 30)
        {
            oldPoints = points;
            return true;
        }
        return false;
    }
}
