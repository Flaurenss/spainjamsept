﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPositionAnnouncer : MonoBehaviour
{
    public WaypointPosition WaypointPos;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            //Debug.Log((int)WaypointPos);
            collision.gameObject.GetComponent<PlayerMovement>().SetPlayerPosition((int)WaypointPos);
        }
    }

    public enum WaypointPosition
    {
        Left,
        LeftMedium,
        RightMedium,
        Right
    }
}
