﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Transform[] Waypoints;
    public delegate void EatFood();
    public event EatFood eatHealthy;
    public event EatFood eatUnhealthy;

    private int waypointsNumber;
    private int currentPos;
    private bool moveLeft;
    private bool moveRight;

    void Start()
    {
        waypointsNumber = Waypoints.Length;
    }

    void Update()
    {
        //Check player movement
        if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            moveLeft = true;
            moveRight = false;
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            moveRight = true;
            moveLeft = false;
        }
        else
        {
            moveRight = false;
            moveLeft = false;
        }
    }

    private void LateUpdate()
    {
        //Move the player
        if(moveRight && currentPos != waypointsNumber - 1)
        {
            transform.position = new Vector3(Waypoints[currentPos+1].position.x, transform.position.y, transform.position.z);
        }
        else if(moveLeft && currentPos != 0)
        {
            transform.position = new Vector3(Waypoints[currentPos-1].position.x, transform.position.y, transform.position.z);
        }
    }
    /// <summary>
    /// Called when the player enters a new food rail
    /// </summary>
    /// <param name="pos"></param>
    public void SetPlayerPosition(int pos)
    {
        currentPos = pos;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Food"))
        {
            //Debug.Log("Ñom");
            //Add points or increase poop value
            if(other.gameObject.GetComponent<FoodBehavior>().HealthyFood)
            {
                eatHealthy();
            }
            else
            {
                eatUnhealthy();
            }
            Destroy(other.gameObject);
        }
    }
}
