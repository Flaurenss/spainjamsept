﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBehavior : MonoBehaviour
{
    public float FoodSpeed;
    public bool HealthyFood;

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(transform.position.x, transform.position.y - FoodSpeed * Time.deltaTime, transform.position.z);
        transform.Translate(Vector3.down * Time.deltaTime * FoodSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Waypoint"))
        {
            Destroy(gameObject);
        }
    }
}
